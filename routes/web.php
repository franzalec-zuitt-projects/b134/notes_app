<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// For creating a note
Route::get('/notes/create', 'NotesController@create');
Route::post('/notes', 'NotesController@store');


// For retrieving notes
Route::get('/notes', 'NotesController@index');
Route::get('/notes/my-notes','NotesController@myNotes');
Route::get('/notes/{note_id}', 'NotesController@show');


// For updating notes
Route::put('/notes/{note_id}', 'NotesController@update');
Route::get('/notes/{note_id}/edit', 'NotesController@edit');

// For Deleting notes
Route::delete('/notes/{note_id}', 'NotesController@destroy');
