<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Notes;

use Auth;

class NotesController extends Controller
{

    // Creating a note


    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create(){
        return view('notes.create');
    }


    public function store(Request $req){
        $new_note = new Notes([
            'title' => $req->input('title'),
            'content' => $req->input('content'),
            'user_id' => Auth::user()->id
        ]);

        // Save it to the database

        $new_note->save();

        // Redirect the user somewhere

        return redirect ('/notes/create');
    }



    // Retrieving all notes

    public function index(){

        $notes_list = Notes::all();

        return view('notes.index')->with('notes', $notes_list);
    }


    // Retrieving specific notes

    public function show($note_id){

        // Retrieve a specific note

        $note = Notes::find($note_id);

        return view('notes.show')->with('note', $note);
    }


    // Retrieving authorized user's notes

    public function myNotes(){

        $my_notes = Auth::user()->notes;

        return view('notes.mynotes')->with('notes', $my_notes);
    }



    // Retrieving specific notes for update

    public function edit($note_id){

        // Find the note to be updated

        $existing_note = Notes::find($note_id);


        // Redirect the user to the page where the note will be updated

        return view('notes.edit')->with('note', $existing_note);
    }


    public function update($note_id, Request $req){

           // Find an existing note to be updated

           $existing_note = Notes::find($note_id);


           // Set the new values of an existing note
   
           $existing_note->title = $req->input('title');
   
           $existing_note->content = $req->input('content');
   
           $existing_note->save();
   
           // Redirect the user to the page of individual note
           return redirect("/notes/$note_id");
    }



    // Deleting a specific note

    public function destroy($note_id){

        // Find the existing post to be deleted
        $existing_note = Notes::find($note_id);

        // Delete the post
        $existing_note->delete();

        // Redirect the user somewhere
        return redirect('/notes/my-notes');
    }
}
