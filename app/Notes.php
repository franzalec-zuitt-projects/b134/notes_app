<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $fillable = [
        'title',
        'content',
        'user_id'
    ];

    // Relationship to User
    public function user(){

        return $this->belongsTo('App\User');
    }
}
